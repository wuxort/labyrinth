#include "Labyrinth.h"
#include "GLUtils.hpp"
#include "Utils.h"

#include <math.h>

Labyrinth::Labyrinth(void)
{
    basicShader = 0;
}

Labyrinth::~Labyrinth(void)
{
}

void Labyrinth::setGLSettings()
{
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_BACK);
}

void Labyrinth::initShaders(GLuint& shader, const char* vertexName, const char* fragmentName)
{
    GLuint vs_ID = loadShader(GL_VERTEX_SHADER,     vertexName);
    GLuint fs_ID = loadShader(GL_FRAGMENT_SHADER,   fragmentName);

    shader = glCreateProgram();

    glAttachShader(shader, vs_ID);
    glAttachShader(shader, fs_ID);

    glBindAttribLocation( shader, 0, "vs_in_pos");
    glBindAttribLocation( shader, 1, "vs_in_col");
    glBindAttribLocation( shader, 2, "vs_in_tex0");

    glLinkProgram(shader);

    glDeleteShader( vs_ID );
    glDeleteShader( fs_ID );
}

bool Labyrinth::isLinkingCorrect()
{
    GLint infoLogLength = 0, result = 0;

    glGetProgramiv(basicShader, GL_LINK_STATUS, &result);
    glGetProgramiv(basicShader, GL_INFO_LOG_LENGTH, &infoLogLength);
    if ( GL_FALSE == result )
    {
        std::vector<char> ProgramErrorMessage( infoLogLength );
        glGetProgramInfoLog(basicShader, infoLogLength, NULL, &ProgramErrorMessage[0]);
        fprintf(stdout, "%s\n", &ProgramErrorMessage[0]);
        return false;
    }
    else
        return true;
}

void Labyrinth::unbindBuffers()
{
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Labyrinth::setPerspective()
{
    m_matProj = glm::perspective( 45.0f, 640/480.0f, 0.01f, 1000.0f );
}

void Labyrinth::connectUniformVariables()
{
    gpu_basic_mvp = glGetUniformLocation( basicShader, "MVP");
    gpu_basic_texture = glGetUniformLocation( basicShader, "texture" );
    gpu_basic_object = glGetUniformLocation( basicShader, "object");
    gpu_basic_objectIT = glGetUniformLocation( basicShader, "objectIT");

    gpu_eye_pos = glGetUniformLocation(specularShader, "eye_pos");
    gpu_suzanne = glGetUniformLocation( specularShader, "suzanne" );
    gpu_suzanneIT = glGetUniformLocation( specularShader, "suzanneIT" );
    gpu_suzanne_mvp = glGetUniformLocation( specularShader, "MVP" );
    gpu_suzanne_tex = glGetUniformLocation( specularShader, "texture" );
}

void Labyrinth::loadTextures()
{
    grassTexture = TextureFromFile("textures/grass.jpg");
    ceilingTexture = TextureFromFile("textures/ceiling.jpg");
    wallTexture = TextureFromFile("textures/wall.jpg");
    beanTexture = TextureFromFile("textures/bean.jpg");
}
void Labyrinth::loadMeshes()
{

    bean = ObjParser::parse("models/sphere.obj");
    bean -> initBuffers();
    writeText("Loaded bean...");
    cow = ObjParser::parse("models/cow.obj.1");
    cow -> initBuffers();
    writeText("Loaded cow...");
    suzanne = ObjParser::parse("models/suzanne.obj");
    suzanne -> initBuffers();
    writeText("Loaded suzanne...");

    loadGroundResources();
    writeText("Loaded ground into buffers...");
    loadWall();
    writeText("Loaded wall into buffers...");
}

void Labyrinth :: loadWall()
{
    Vertex floor_vertices[] =
    {
        {glm::vec3( 0, 0, 0), glm::vec3( 0, 1, 0), glm::vec2(0, 0)},
        {glm::vec3( 0, 1, 0), glm::vec3( 0, 1, 0), glm::vec2(0, 1)},
        {glm::vec3( 1, 0, 0), glm::vec3( 0, 1, 0), glm::vec2(1, 0)},
        {glm::vec3( 1, 1, 0), glm::vec3( 0, 1, 0), glm::vec2(1, 1)},
    };

    GLuint floor_indices[] =
    {
        0,1,3,2,
        2,3,1,0,
    };

    wall = new Mesh();
    wall -> setVertices(floor_vertices, sizeof(floor_vertices));
    wall -> setIndices(floor_indices, sizeof(floor_indices));
    wall -> initBuffers();
}

void Labyrinth :: drawWall(float x, float z, int facing)
{
    float face = (facing == 1 ? 90 : 0);
    glm::mat4 walltransform = glm::translate<float>(glm::vec3(x,0,z)) *
                              glm::rotate<float>(face, 0, 1, 0);
    drawMesh(wall, wallTexture, walltransform, GL_QUADS, basicShader);
}

void Labyrinth :: initWallCoordinates()
{
    wallCoordinates.push_back(glm::vec3( 3, -3, 0));
    wallCoordinates.push_back(glm::vec3( 2, -3, 0));
    wallCoordinates.push_back(glm::vec3( 0, -3, 0));
    wallCoordinates.push_back(glm::vec3(-1, -3, 0));
    wallCoordinates.push_back(glm::vec3(-2, -3, 1));

    wallCoordinates.push_back(glm::vec3( 2, -2, 1));
    wallCoordinates.push_back(glm::vec3( 1, -2, 1));
    wallCoordinates.push_back(glm::vec3(-1, -2, 1));
    wallCoordinates.push_back(glm::vec3(-2, -2, 1));
    wallCoordinates.push_back(glm::vec3(-3, -2, 1));
    wallCoordinates.push_back(glm::vec3(-4, -2, 0));

    wallCoordinates.push_back(glm::vec3( 3, -1, 0));
    wallCoordinates.push_back(glm::vec3( 3, -1, 1));
    wallCoordinates.push_back(glm::vec3( 2, -1, 0));
    wallCoordinates.push_back(glm::vec3( 1, -1, 0));
    wallCoordinates.push_back(glm::vec3( 1, -1, 1));
    wallCoordinates.push_back(glm::vec3( 0, -1, 1));
    wallCoordinates.push_back(glm::vec3(-1, -1, 1));
    wallCoordinates.push_back(glm::vec3(-3, -1, 0));
    wallCoordinates.push_back(glm::vec3(-3, -1, 1));

    wallCoordinates.push_back(glm::vec3( 1,  0, 0));
    wallCoordinates.push_back(glm::vec3( 0,  0, 1));
    wallCoordinates.push_back(glm::vec3( 0,  0, 0));
    wallCoordinates.push_back(glm::vec3(-1,  0, 0));
    wallCoordinates.push_back(glm::vec3(-2,  0, 0));
    wallCoordinates.push_back(glm::vec3(-2,  0, 1));

    wallCoordinates.push_back(glm::vec3( 3,  1, 1));
    wallCoordinates.push_back(glm::vec3( 2,  1, 1));
    wallCoordinates.push_back(glm::vec3( 0,  1, 0));
    wallCoordinates.push_back(glm::vec3(-2,  1, 0));
    wallCoordinates.push_back(glm::vec3(-2,  1, 1));
    wallCoordinates.push_back(glm::vec3(-3,  1, 1));

    wallCoordinates.push_back(glm::vec3( 3,  2, 1));
    wallCoordinates.push_back(glm::vec3( 2,  2, 1));
    wallCoordinates.push_back(glm::vec3( 1,  2, 0));
    wallCoordinates.push_back(glm::vec3( 0,  2, 0));
    wallCoordinates.push_back(glm::vec3( 0,  2, 1));
    wallCoordinates.push_back(glm::vec3(-2,  2, 0));
    wallCoordinates.push_back(glm::vec3(-3,  2, 0));
    wallCoordinates.push_back(glm::vec3(-3,  2, 1));

    wallCoordinates.push_back(glm::vec3( 2,  3, 0));
    wallCoordinates.push_back(glm::vec3( 1,  3, 0));
    wallCoordinates.push_back(glm::vec3( 0,  3, 0));
    wallCoordinates.push_back(glm::vec3(-1,  3, 0));
    wallCoordinates.push_back(glm::vec3(-1,  3, 1));
    wallCoordinates.push_back(glm::vec3(-2,  3, 1));
    wallCoordinates.push_back(glm::vec3(-4,  3, 0));

    wallCoordinates.push_back(glm::vec3(-2,  4, 1));
}

void Labyrinth :: drawWalls()
{
    for (int i = 0; i < wallCoordinates.size(); i++)
    {
        drawWall(wallCoordinates[i].x, wallCoordinates[i].y, wallCoordinates[i].z);
    }
}

void Labyrinth::loadGroundResources()
{
    Vertex floor_vertices[] =
    {
        {glm::vec3(-4, 0, -4), glm::vec3( 0, 1, 0), glm::vec2(0, 0)},
        {glm::vec3(-4, 0,  4), glm::vec3( 0, 1, 0), glm::vec2(0, 1)},
        {glm::vec3( 4, 0, -4), glm::vec3( 0, 1, 0), glm::vec2(1, 0)},
        {glm::vec3( 4, 0,  4), glm::vec3( 0, 1, 0), glm::vec2(1, 1)},
    };

    GLuint floor_indices[] =
    {
        0,1,3,2,
        2,3,1,0,
    };

    floor = new Mesh();
    floor->setVertices(floor_vertices, sizeof(floor_vertices));
    floor->setIndices(floor_indices, sizeof(floor_indices));
    floor->initBuffers();
}

void Labyrinth::drawMesh(Mesh* mesh, GLuint texture, glm::mat4 transformation, int method, GLuint shader)
{
    glUseProgram( shader );

    m_matWorld = glm::translate<float>(0, 1, 0);
    glm::mat4 mvp = m_matProj * m_matView * transformation;
    glUniformMatrix4fv( gpu_basic_mvp, 1, GL_FALSE, &(mvp[0][0]) );

    glm::mat4 transformationIT = glm::transpose( glm::inverse(transformation) );

    glUniformMatrix4fv( gpu_basic_object, 1, GL_FALSE, &(transformation[0][0]));
    glUniformMatrix4fv( gpu_basic_objectIT, 1, GL_FALSE, &(transformationIT[0][0]));

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(gpu_basic_texture, 0);

    mesh->draw(method);

    glUseProgram( 0 );
}

void Labyrinth :: drawSuzanne()
{
    glUseProgram( specularShader );

    glm::mat4 suzannetransform = glm::translate<float>(suzannePosition) * glm::mat4(1.0f) *
                              glm::scale<float>(glm::vec3(0.1, 0.1, 0.1));
    glm::mat4 suzannetransformIT = glm::transpose( glm::inverse( suzannetransform ) );

    glUniformMatrix4fv( gpu_suzanne, 1, GL_FALSE, &(suzannetransform[0][0]) );
    glUniformMatrix4fv( gpu_suzanneIT, 1, GL_FALSE, &(suzannetransformIT[0][0]) );

    glm::mat4 mvp = m_matProj * m_matView * suzannetransform;
    glUniformMatrix4fv( gpu_suzanne_mvp, 1, GL_FALSE, &(mvp[0][0]) );

    glUniform3f(gpu_eye_pos, player -> getPosition().x, player -> getPosition().y, player -> getPosition().z);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, wallTexture);
    glUniform1i(gpu_suzanne_tex, 0);

    suzanne -> draw(GL_TRIANGLES);

    glUseProgram( 0 );
}

void Labyrinth::drawGround()
{
    glm::mat4 notransform = glm::scale<float>(glm::vec3(1, 1, 1));
    glm::mat4 transform_one_up = glm::translate<float>(glm::vec3(0, 1, 0));
    glm::mat4 left_wall_transform = glm::translate<float>(glm::vec3(4, 0.5, 0)) *
                                    glm::rotate<float>(90,0,0,1) *
                                    glm::scale<float>(0.125, 1, 1);
    glm::mat4 right_wall_transform = glm::translate<float>(glm::vec3(-4, 0.5, 0)) *
                                    glm::rotate<float>(90,0,0,1) *
                                    glm::scale<float>(0.125, 1, 1);
    glm::mat4 front_wall_transform = glm::translate<float>(glm::vec3(0.5, 0.5, 4)) *
                                    glm::rotate<float>(90,0,0,1) *
                                    glm::scale<float>(0.125, 0.875, 1) *
                                    glm::rotate<float>(90, 1, 0, 0);
    glm::mat4 back_wall_transform = glm::translate<float>(glm::vec3(0, 0.5, -4)) *
                                    glm::rotate<float>(90,0,0,1) *
                                    glm::scale<float>(0.125, 1, 1) *
                                    glm::rotate<float>(90, 1, 0, 0);
    drawMesh(floor, grassTexture, notransform, GL_QUADS, basicShader);
    drawMesh(floor, ceilingTexture, transform_one_up, GL_QUADS, basicShader);
    drawMesh(floor, wallTexture, left_wall_transform, GL_QUADS, basicShader);
    drawMesh(floor, wallTexture, right_wall_transform, GL_QUADS, basicShader);
    drawMesh(floor, wallTexture, front_wall_transform, GL_QUADS, basicShader);
    drawMesh(floor, wallTexture, back_wall_transform, GL_QUADS, basicShader);
}

void Labyrinth :: drawBean()
{
    glm::mat4 beantransform = glm::translate<float>(beanPosition) *
                              glm::scale<float>(glm::vec3(0.2, 0.2, 0.2));
    drawMesh(bean, beanTexture, beantransform, GL_TRIANGLES, basicShader);
}

void Labyrinth :: drawCow()
{
    glm::mat4 cow1transform = glm::translate<float>(cow1Position) *
                              glm::rotate<float>(-cow1Orientation, 0, 1, 0) *
                              glm::scale<float>(glm::vec3(0.05, 0.05, 0.05));
    drawMesh(cow, beanTexture, cow1transform, GL_TRIANGLES, basicShader);

    glm::mat4 cow2transform = glm::translate<float>(cow2Position) *
                              glm::rotate<float>(cow2Orientation, 0, 1, 0) *
                              glm::scale<float>(glm::vec3(0.05, 0.05, 0.05));
    drawMesh(cow, ceilingTexture, cow2transform, GL_TRIANGLES, basicShader);
}


bool Labyrinth :: isGameEnd()
{
    if (player->getPosition().x < -3 && player->getPosition().z > 3)
        return true;
    else
        return false;
}


bool Labyrinth :: isCollisionWithCow()
{
    if (getDistance(cow1Position, player -> getPosition()) < 0.4 ||
        getDistance(cow2Position, player -> getPosition()) < 0.4)
    {
        return true;
    }
    return false;
}

bool Labyrinth :: isCollisionWithSuzanne()
{
    if (getDistance(suzannePosition, player -> getPosition()) < 0.2)
        return true;
    else
        return false;
}

bool Labyrinth :: isCollisionWithBean()
{
    if (getDistance(beanPosition, player -> getPosition()) < 0.7)
    {
        beanPosition.x = (float)(rand() % 7 + 1) - 3.5;
        beanPosition.z = (float)(rand() % 7 + 1) - 3.5;
        beanPosition.y = 0.3;
        return true;
    }
    return false;
}

bool Labyrinth :: across(glm::vec3& cowPosition)
{
    bool localbool = false;
    if ((cowPosition.x < 0.5 && cowPosition.x > -0.5) ||
        (cowPosition.z < 0.5 && cowPosition.z > -0.5))
    {
        localbool = true;
    }
    return localbool;
}

void Labyrinth :: moveCow(glm::vec3& cowPosition, float& cowOrientation, bool& log, bool& isCowTurning, float& startori, float& endori)
{
    if (!isCowTurning)
    {
        cowPosition.x += cos(radianToDegree(cowOrientation)) / 200;
        cowPosition.z += sin(radianToDegree(cowOrientation)) / 200;
        if (!log)
        {
            log = across(cowPosition);
        }
        else
        {
            if (cowPosition.z < -3.8 || cowPosition.z > 3.8 ||
                cowPosition.x < -3.8 || cowPosition.x > 3.8)
            {
                isCowTurning = true;
                log = false;
                startori = cowOrientation;
                endori = startori + 180;
            }
        }
    }
    else
    {
        if (startori < endori)
        {
            startori +=  180.0/5000*(stime-gameStartTime);
            cowOrientation += 180.0/5000*(stime-gameStartTime);
        }
        else
        {
            isCowTurning = false;
        }
    }
}








bool Labyrinth :: Init()
{
    glClearColor(0.125f, 0.25f, 0.5f, 1.0f);

    setGLSettings();
    initShaders(basicShader, "basic.vert", "basic.frag");
    initShaders(specularShader, "specular.vert", "specular.frag");

    if (!isLinkingCorrect())
        return false;

    setPerspective();
    connectUniformVariables();
    loadTextures();
    loadMeshes();
    initWallCoordinates();

    srand(time(NULL));

    glm::vec3 position = glm::vec3(3.5,0.5,-3.5);
    player = new Player(position);

    beanPosition = glm::vec3(0.5, 0.3, 0.5);
    cow1Position = glm::vec3(-3.5, 0.2, 0.5);
    cow2Position = glm::vec3(-1.5, 0.2,-0.5);
    cow1Orientation = -90.0f;
    cow2Orientation = 0.0f;
    suzannePosition = glm::vec3(-0.5, 0.5, 0.5);

    beanPowerUp = true;
    log = false;
    log2 = false;

    startori1 = 0;
    startori2 = 0;
    gameStartTime = 0;
    isCow1Turning = false;
    isCow2Turning = false;
    suzanneUpOrDown = 1;

    return true;
}




void Labyrinth :: Update()
{
    stime = SDL_GetTicks();

    m_matView = player->view();
    if (isGameEnd()) { exit(0); }
    if (!beanPowerUp)
    {
        if (isCollisionWithCow())
        {
            std::cout << "Tehennel utkoztel! Jatek vege!" << std::endl;
            exit(0);
        }
        if (isCollisionWithSuzanne())
        {
            std::cout << "Suzannel utkoztel! Jatek vege!" << std::endl;
            exit(0);
        }
    }
    if (isCollisionWithBean())
    {
        startTime = SDL_GetTicks();
        beanPowerUp = true;
        std::cout << "BEAN POWERUP!!!" << std::endl;
    }
    if (SDL_GetTicks() > startTime+30000)
    {
        beanPowerUp = false;
    }

    moveCow(cow1Position, cow1Orientation, log, isCow1Turning, startori1, endori1);
    moveCow(cow2Position, cow2Orientation, log2, isCow2Turning, startori2, endori2);

    suzannePosition.y += 0.02 * suzanneUpOrDown;
    if ((suzannePosition.y >= 0.8) || (suzannePosition.y <= 0.2))
        suzanneUpOrDown *= -1;
}






void Labyrinth :: Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    drawGround();
    drawBean();
    drawCow();
    drawSuzanne();
    drawWalls();

    gameStartTime = stime;
}





void Labyrinth::KeyboardForward() { player->moveForward(); }
void Labyrinth::KeyboardBackward() { player->moveBackward(); }
void Labyrinth::KeyboardLeft() { player->turn(5); }
void Labyrinth::KeyboardRight() { player->turn(-5); }
void Labyrinth::KeyboardShoot(){}
void Labyrinth::KeyboardUpward() {}
void Labyrinth::KeyboardDownward() {}

void Labyrinth::MouseMove(SDL_MouseMotionEvent& mouse){}
void Labyrinth::MouseDown(SDL_MouseButtonEvent& mouse){}
void Labyrinth::MouseUp(SDL_MouseButtonEvent& mouse){}
void Labyrinth::MouseWheel(SDL_MouseWheelEvent& wheel){}

void Labyrinth::Resize(int width, int height)
{
    glViewport(0, 0, width, height);
    m_matProj = glm::perspective(  45.0f, // 90 fokos nyilasszog
                                   width/(float)height,	// ablakmereteknek megfelelo nezeti arany
                                   0.01f,       // kozeli vagosik
                                   100.0f);    // tavoli vagosik
}

void Labyrinth::Clean()
{
    glDeleteTextures(1, &grassTexture);
    glDeleteTextures(1, &wallTexture);
    glDeleteTextures(1, &ceilingTexture);
    glDeleteTextures(1, &beanTexture);

    glDeleteProgram( basicShader );
    glDeleteProgram( specularShader );
}

void Labyrinth :: writeText(std::string text)
{
    std::cout << "[" << SDL_GetTicks() / 1000.0 << " sec] " << text << std::endl;
}
