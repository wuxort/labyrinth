#include "Utils.h"

float getDistance(glm::vec3 position1, glm::vec3 position2)
{
    glm::vec3 temp = position1 - position2;
    return glm::dot(temp, temp);
}

float radianToDegree(int radian)
{
    return radian * 3.14 / 180;
}
