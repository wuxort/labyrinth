#version 130

in vec3 vs_out_pos;
in vec3 vs_out_normal;
in vec2 vs_out_tex0;

out vec4 fs_out_col;

uniform sampler2D texture;

uniform vec4 Light_ambient = vec4(0.1f, 0.1f, 0.1f, 1);
uniform vec4 Kezdo_ambient = vec4(1, 1, 1, 1);

uniform vec4 Light_diffuse = vec4(0.7f, 0.7f, 0.7f, 1);
uniform vec4 Kezdo_diffuse = vec4(0.75f, 0.25f, 0.125f, 1);

uniform vec3 Light_position = vec3(0, 0.8, 0);
uniform vec4 Light_specular = vec4(1, 1, 1, 1);
uniform vec4 Kezdo_specular = vec4(1, 0, 0, 1);
uniform float specular_power = 16;

uniform vec3 eye_pos;

void main()
{
    vec4 ambient = Light_ambient * Kezdo_ambient;


    vec3 normal = normalize( vs_out_normal );
    vec3 toLight = normalize(Light_position - vs_out_pos);
    float diffuse_part = clamp( dot( toLight, normal), 0.0f, 1.0f );
    vec4 diffuse = Light_diffuse * Kezdo_diffuse * diffuse_part;

    vec3 e = normalize( eye_pos - vs_out_pos );
    vec3 r = reflect( -toLight, normal );
    float si = pow( clamp( dot(e, r), 0.0f, 1.0f ), specular_power );
    vec4 specular = Light_specular * Kezdo_specular * si;

    fs_out_col = (ambient + diffuse + specular) * texture2D(texture, vs_out_tex0.st);
}
