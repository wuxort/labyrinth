#include "Mesh.h"
#include <iostream>

Mesh::Mesh(void)
{
}

Mesh::~Mesh(void)
{
}

void Mesh::initBuffers()
{
    initVertexBuffers();
    initIndexBuffers();
	glBindVertexArray(0);
}

void Mesh::setVertices(Vertex inc_vertices[], GLuint size)
{
    for (int index = 0; index < size/sizeof(Vertex); index++)
    {
        Vertex v;
        v.position = inc_vertices[index].position;
        v.normal = inc_vertices[index].normal;
        v.texcoord = inc_vertices[index].texcoord;
        addVertex(v);
    }
}

void Mesh::setIndices(GLuint inc_indices[], GLushort size)
{
    for (int index = 0; index < size/sizeof(GLuint); index++)
    {
        addIndex(inc_indices[index]);
    }
}

void Mesh::initIndexBuffers()
{
	glGenBuffers(1, &indexBufferObjectID);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferObjectID);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(int)*indices.size(), (void*)&indices[0], GL_STREAM_DRAW);
}

void Mesh::initVertexBuffers()
{
	glGenVertexArrays(1, &vertexArrayObjectID);
	glGenBuffers(1, &vertexBufferObjectID);

	glBindVertexArray(vertexArrayObjectID);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObjectID);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex)*vertices.size(), (void*)&vertices[0], GL_STREAM_DRAW);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), 0);
	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)));
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)(sizeof(glm::vec3)*2));
}

void Mesh::draw(int method)
{
	glBindVertexArray(vertexArrayObjectID);
	glDrawElements(method, indices.size(), GL_UNSIGNED_INT, 0);
	glBindVertexArray(0);
}

void Mesh::addVertex(const Vertex& vertex)
{
    vertices.push_back(vertex);
}

void Mesh::addIndex(unsigned int index)
{
    indices.push_back(index);
}
