#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

class Player
{
public:
    Player( glm::vec3 startPosition );
    ~Player();

    glm::vec3 getPosition();
    glm::mat4 view();

    void moveForward();
    void moveBackward();
    void turn(float amount);

private:

    glm::vec3 cameraPosition;
    glm::vec3 lastCameraPosition;
    glm::vec3 cameraTarget;

    float cameraRotation;

    bool isPlayerOutside();

    void move(int where);
};
