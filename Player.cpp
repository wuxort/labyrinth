#include "Player.h"
#include "Utils.h"

Player :: Player (glm::vec3 startPosition)
{
    cameraPosition = startPosition;
    lastCameraPosition = cameraPosition;
    cameraRotation = 0.0f;
}

Player :: ~Player(){}

void Player :: move (int where)
{
    lastCameraPosition = cameraPosition;
    cameraPosition.x += sin(radianToDegree(cameraRotation)) / 12 * where;
    cameraPosition.z += cos(radianToDegree(cameraRotation)) / 12 * where;
    if (isPlayerOutside())
    {
        cameraPosition = lastCameraPosition;
    }
}

void Player :: moveForward()  { this->move( 1); }
void Player :: moveBackward() { this->move(-1); }

void Player :: turn (float amount)
{
    cameraRotation += amount;
}

glm::vec3 Player :: getPosition() { return cameraPosition; }


glm::mat4 Player :: view()
{
    cameraTarget = cameraPosition + glm::vec3(sin(radianToDegree(cameraRotation)), 0, cos(radianToDegree(cameraRotation)));
    return glm::lookAt(cameraPosition, cameraTarget, glm::vec3(0,  1,  0));
}

bool Player :: isPlayerOutside()
{
    if (cameraPosition.x > 3.9 || cameraPosition.x < -3.9 ||
        cameraPosition.z > 3.9 || cameraPosition.z < -3.9)
        return true;
    else
        return false;
}

