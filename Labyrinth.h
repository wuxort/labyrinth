#pragma once

#include <GL/glew.h>

#include <SDL.h>
#include <SDL_opengl.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform2.hpp>

#include "ObjectParser.h"
#include <vector>

#include "Vertex.h"

#include "Player.h"

class Labyrinth
{
public:
    Labyrinth(void);
    ~Labyrinth(void);

    bool Init();
    void Clean();

    void Update();
    void Render();

    void KeyboardForward();
    void KeyboardBackward();
    void KeyboardLeft();
    void KeyboardRight();
    void KeyboardDownward();
    void KeyboardUpward();
    void KeyboardShoot();
    void MouseMove(SDL_MouseMotionEvent&);
    void MouseDown(SDL_MouseButtonEvent&);
    void MouseUp(SDL_MouseButtonEvent&);
    void MouseWheel(SDL_MouseWheelEvent&);
    void Resize(int, int);
protected:
    glm::mat4 m_matWorld;
    glm::mat4 m_matView;
    glm::mat4 m_matProj;

private:
    GLuint basicShader;
    GLuint specularShader;

    GLuint gpu_eye_pos;
    GLuint gpu_suzanne;
    GLuint gpu_suzanneIT;
    GLuint gpu_suzanne_mvp;
    GLuint gpu_suzanne_tex;

    GLuint gpu_basic_mvp;
    GLuint gpu_basic_object;
    GLuint gpu_basic_objectIT;
    GLuint gpu_basic_texture;

    GLuint grassTexture;
    GLuint ceilingTexture;
    GLuint wallTexture;
    GLuint beanTexture;

    Mesh* bean;
    Mesh* cow;
    Mesh* floor;
    Mesh* suzanne;
    Mesh* wall;

    Player* player;

    glm::vec3 beanPosition;
    glm::vec3 cow1Position;
    glm::vec3 cow2Position;
    glm::vec3 suzannePosition;
    int suzanneUpOrDown;

    bool beanPowerUp;
    bool isCow1Turning;
    bool isCow2Turning;

    float startTime;
    float cow1Orientation;
    float cow2Orientation;
    float startori1;
    float startori2;
    float endori1;
    float endori2;

    std::vector<glm::vec3> wallCoordinates;

    bool log;
    bool log2;
    float gameStartTime;
    float stime;
    float framesPerSec;

    void setGLSettings();
    void initShaders(GLuint& shader, const char* vertexName, const char* fragmentName);
    bool isLinkingCorrect();
    void unbindBuffers();
    void setPerspective();
    void connectUniformVariables();
    void loadTextures();
    void loadMeshes();

    void loadGroundResources();

    void drawGround();
    void drawBean();
    void drawCow();
    void drawSuzanne();
    void loadWall();
    void initWallCoordinates();
    void drawWall(float x, float z, int facing);
    void drawWalls();
    void moveCow(glm::vec3& cowPosition, float& cowOrientation, bool& log, bool& isCowTurning, float& startori, float& endori);
    void drawMesh(Mesh* mesh, GLuint texture, glm::mat4 transformation, int method, GLuint shader);

    void KeyboardUp(SDL_KeyboardEvent&);

    bool across(glm::vec3& cowPosition);
    bool isGameEnd();
    bool isCollisionWithCow();
    bool isCollisionWithBean();
    bool isCollisionWithSuzanne();

    void writeText(std::string text);
};
