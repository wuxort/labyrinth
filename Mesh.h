#pragma once

#include <GL/glew.h>

#include <vector>
#include <glm/glm.hpp>

#include "Vertex.h"

class Mesh
{
public:

    Mesh(void);
    ~Mesh(void);

    void initBuffers();
    void draw(int method);

    void addVertex(const Vertex& vertex);
    void addIndex(unsigned int index);

    void setVertices(Vertex vertices[], GLuint size);
    void setIndices(GLuint indices[], GLushort size);
private:
    GLuint vertexArrayObjectID;
    GLuint vertexBufferObjectID;
    GLuint indexBufferObjectID;

    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;

    void initIndexBuffers();
    void initVertexBuffers();
};
