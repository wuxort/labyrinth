#ifndef __UTILS__H__
#define __UTILS__H__

#include <glm/glm.hpp>

float getDistance(glm::vec3 position1, glm::vec3 position2);
float radianToDegree(int radian);

#endif
